
// Playing Cards
// Paul Haller
// Jason Clemons

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card c1;
	c1.Rank = Rank::SEVEN;
	c1.Suit = Suit::DIAMONDS;

	Card c2;
	c2.Rank = Rank::FOUR;
	c2.Suit = Suit::HEARTS;

	// print high card for testing.
	PrintCard(HighCard(c1,c2));


	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	// set suit string to print
	string suitOut = "";
	switch (card.Suit)	{
		case Suit::SPADES:
			suitOut = "Spades";	break;
		case Suit::DIAMONDS:
			suitOut = "Diamonds"; break;
		case Suit::HEARTS:
			suitOut = "Hearts";	break;
		case Suit::CLUBS:
			suitOut = "Clubs";	break;
	}

	// find value of rank
	int rankVal = 0;
	string rankOut = "";
	for (int i = 0; i <= 14; ++i){
		if (card.Rank == (Rank)i) { rankVal = i; };
	};

	// print out rank and suit of card 
	switch (rankVal){
		case 11:
			cout << "Jack of " << suitOut; break;
		case 12:
			cout << "Queen of " << suitOut;	break;
		case 13:
			cout << "King of " << suitOut; break;
		case 14:
			cout << "Ace of " << suitOut; break;
		default: 
			cout << rankVal << " of " << suitOut;
	}
};

Card HighCard(Card card1, Card card2) 
{
	// find card values return highest
	// picks card1 on a tie
	for (int i = 14; i > 0; --i)
	{
		if (card1.Rank == (Rank)i) { return card1; };
		if (card2.Rank == (Rank)i) { return card2; };
	};
};